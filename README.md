# Viewer 3.0

Backend for [viewer 3.0 application](https://gitlab.com/viewerus/viewer3-frontend). Written in typescript using [NestJS](https://github.com/nestjs/nest) framework.


## Technologies
- **Framework:** [NestJs](https://nestjs.com/)
- **Database:** [PostgreSQL](http://www.postgresql.org.pl/)
- **ORM:** [TypeORM](http://typeorm.io/#/)
- **Documentation:** [swagger](https://swagger.io/)
- **Validation:** [class-validator](https://github.com/typestack/class-validator)
- **Authentication:** [passport](http://www.passportjs.org/packages/passport-jwt-cookiecombo/) with [jsonwebtoken](https://jwt.io/)
- **Password hashing:** [bcrypt](https://www.npmjs.com/package/bcryptjs)
- **Virtualization:** [docker](https://www.docker.com/)
- **Server:** [NGINX](https://www.nginx.com/)
- **SSL:** [Let’s Encrypt](https://letsencrypt.org/) and [Certbot](https://certbot.eff.org/)


## Features

#### Server
- Error and route logging
- Documentation
- Params validation
- Route authorization (session, roles)
- Sending emails with html templates
- Complete http server with reverse proxy
- Automated SSL certificate

#### Accounts
- Login
- Register
- Activation (email)
- Password reset
- Roles
- Blocking
- Email resending

#### Projects
- List
- Create
- Delete
- Details
- Get related files


## Environment

To run this app docker and docker-compose have to be installed along with git.

**Install Git**
* `sudo apt update && sudo apt install git`

**Install Docker**
* `curl -fsSL https://get.docker.com -o get-docker.sh`
* `sudo sh get-docker.sh`
* `sudo usermod -aG docker $USER`
* `rm get-docker.sh`
* `sudo systemctl enable docker`

**Install Docker-Compose**
* `sudo curl -L "https://github.com/docker/compose/releases/download/1.23.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose`
* `sudo chmod +x /usr/local/bin/docker-compose`

## Local deployment

To run app locally inside a docker container and see logs after it's started run:

1. Clone this repository `git clone git@gitlab.com:viewerus/viewer3-backend.git`
2. Enter project directory: `cd viewer3-backend`
3. Run application: `docker-compose up -d --force-recreate --build node && docker-compose logs -f -t`
4. Visit the docs: `http://localhost:3000/api/v1/docs/`

## Production deployment

Before you deploy the application be sure to replace the variables in .env and app.env files with you target values!

* git clone git clone git@gitlab.com:viewerus/viewer3-backend.git
* cd viewer3-backend
* docker-compose up

## Development
run the application locally:

1. Clone this repository `git clone git@gitlab.com:viewerus/viewer3-backend.git`
2. Enter project directory: `cd viewer3-backend`
3. Install dependencies: `npm i`
4. Run application with: `npm start`
5. To see docs visit: `http://localhost:3000/api/v1/docs/`


## Feedback
I will be more than happy to receive some feedback, ideas and constructive criticism. 
If you want to share some thoughts just create an issue in this repository. 
