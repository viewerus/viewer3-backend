import { Injectable, Logger, UnauthorizedException } from '@nestjs/common';
import { AuthService } from '../auth.service';
import { PassportStrategy } from '@nestjs/passport';
import { ExtractJwt, Strategy } from 'passport-jwt';
import { Configuration } from '../../../shared/configuration/configuration';
import { UserService } from '../../user/user.service';

const jwtOptions = {
  jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
  secretOrKey: Configuration.sessionSecret,
  algorithms: ['HS512'],
};

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
  constructor(
    private readonly authService: AuthService,
    private readonly userService: UserService,
  ) {
    super(jwtOptions);
  }

  async validate(payload) {
    Logger.log(`Validation with: ${JSON.stringify(payload)}`, 'Passport');
    let user;
    try {
      user = await this.userService.getByEmail(payload.email);
      Logger.log(`Found user ${JSON.stringify(user)}`, 'Passport');
    } catch (e) {
      Logger.log(`Error with token ${JSON.stringify(e)}`, 'Passport');
      throw new UnauthorizedException();
    }
    return user;
  }
}
