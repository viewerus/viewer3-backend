import { Body, Controller, Logger, Post } from '@nestjs/common';
import { ApiUseTags, ApiOperation, ApiResponse, ApiOkResponse } from '@nestjs/swagger';
import { AuthService } from './auth.service';
import { LoginDto } from './models/dto/login.dto';
import { LoginResponse } from './models/dto/login.response';
import { RegisterDto } from './models/dto/register.dto';
import { ApiSuccessDto } from '../../shared/models/api-success.dto';
import { TokenDto } from './models/dto/token.dto';
import { RecoveryDto } from './models/dto/recovery.dto';
import { PasswordResetDto } from './models/dto/password-reset.dto';
import { EmailDto } from './models/dto/email.dto';

@Controller('auth')
@ApiUseTags('Auth')
export class AuthController {
  constructor( private readonly service: AuthService ) {}

  @Post('login')
  @ApiOkResponse({ type: LoginResponse })
  @ApiResponse({ status: 200, description: 'User Found.'})
  @ApiResponse({ status: 400, description: 'Invalid username or password.'})
  @ApiResponse({ status: 403, description: 'Account not activated.'})
  @ApiResponse({ status: 423, description: 'Account blocked by admin.'})
  @ApiResponse({ status: 429, description: 'Too many failed login attempts.'})
  @ApiOperation({
      title: 'Exchange credentials for session token.',
      operationId: 'Auth_Login',
  })
  async login(@Body() credentials: LoginDto): Promise<{token: string}> {
    return await this.service.login(credentials);
  }

  @Post('register')
  @ApiOkResponse({ type: ApiSuccessDto })
  @ApiResponse({ status: 201, description: 'User created.'})
  @ApiResponse({ status: 400, description: 'Invalid request.'})
  @ApiResponse({ status: 409, description: 'User already exists.'})
  @ApiOperation({
    title: 'Create new account.',
    operationId: 'Auth_Register',
  })
  async register(@Body() registerDto: RegisterDto) {
    return await this.service.register(registerDto);
  }

  @Post('activate')
  @ApiOkResponse({ type: ApiSuccessDto })
  @ApiResponse({ status: 200, description: 'Account activated.'})
  @ApiResponse({ status: 400, description: 'Invalid request/token.'})
  @ApiOperation({
    title: 'Confirm email address and activate account.',
    operationId: 'Auth_Activate',
  })
  async activate(@Body() body: TokenDto) {
    Logger.log(`Got token ${body.token}`, 'activate');
    return await this.service.activate(body.token);
  }

  @Post('recover-account')
  @ApiResponse({ status: 200, description: 'Request received.'})
  @ApiOperation({
    title: 'Send email with password-reset token to the user.',
    operationId: 'Auth_Recover',
  })
  async recoverAccount(@Body() body: RecoveryDto): Promise<ApiSuccessDto> {
    return await this.service.recoverAccount(body.email);
  }

  @Post('reset-password')
  @ApiResponse({ status: 200, description: 'Request received.'})
  @ApiOperation({
    title: 'Change account password based on email token.',
    operationId: 'Auth_Reset',
  })
  async resetPassword(@Body() body: PasswordResetDto) {
    return await this.service.resetPassword(body);
  }

  @Post('resend-activation-email')
  @ApiResponse({ status: 200, description: 'Request received.'})
  @ApiOperation({
    title: 'Resend account activation email.',
    operationId: 'Auth_ResendActivation',
  })
  async resendActivation(@Body() body: EmailDto) {
    return await this.service.resendActivation(body.email);
  }

  @Post('resend-recovery-email')
  @ApiResponse({ status: 200, description: 'Request received.'})
  @ApiOperation({
    title: 'Resend account recovery email.',
    operationId: 'Auth_ResendRecovery',
  })
  async resendRecovery(@Body() body: EmailDto) {
    return await this.service.resendRecovery(body.email);
  }

}
