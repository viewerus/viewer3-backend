import { Module } from '@nestjs/common';
import { JwtModule } from '@nestjs/jwt';
import { PassportModule } from '@nestjs/passport';

import { AuthController } from './auth.controller';
import { AuthService } from './auth.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { User } from '../user/models/entities/user.entity';
import { Status } from '../user/models/entities/status.entity';
import { Role } from '../user/models/entities/role.entity';
import { UserModule } from '../user/user.module';
import { JwtStrategy } from './passport/jwt.strategy';
import { Configuration } from '../../shared/configuration/configuration';

@Module({
  controllers: [AuthController],
  imports: [
    TypeOrmModule.forFeature([Role, User, Status]),
    UserModule,
    PassportModule.register({ defaultStrategy: 'jwt' }),
    JwtModule.register({
      secretOrPrivateKey: Configuration.sessionSecret,
      signOptions: {
        expiresIn: Configuration.sessionDuration,
      },
    }),
  ],
  providers: [AuthService, JwtStrategy],
})
export class AuthModule {}
