import { HttpException, HttpStatus, Inject, Injectable, Logger } from '@nestjs/common';
import { User } from '../user/models/entities/user.entity';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { RegisterDto } from './models/dto/register.dto';
import { plainToClass } from 'class-transformer';
import { RegisterResponse } from './models/dto/register.response';
import { Status } from '../user/models/entities/status.entity';
import { Configuration } from '../../shared/configuration/configuration';
import { Role } from '../user/models/entities/role.entity';
import * as bcrypt from 'bcryptjs';
import * as jwt from 'jsonwebtoken';
import { MailerService } from '@nest-modules/mailer';
import { ApiSuccessDto } from '../../shared/models/api-success.dto';

@Injectable()
export class AuthService {
  public readonly loginAttemptsLimit: number;
  public readonly lockTime: number;
  public readonly sessionDuration: number;

  constructor(
    @InjectRepository(User)
    private userRepository: Repository<User>,
    @InjectRepository(Status)
    private statusRepository: Repository<Status>,
    @InjectRepository(Role)
    private roleRepository: Repository<Role>,
    @Inject('MailerService')
    private readonly mailerService: MailerService,
  ) {
    this.loginAttemptsLimit = Configuration.loginAttemptsLimit;
    this.sessionDuration = Configuration.sessionDuration;
    this.lockTime = Configuration.lockTime;
  }

  /**
   * Exchange credentials for stateless auth token
   * @param email
   * @param password
   */
  async login({email, password}) {
    Logger.log(`Logging in... ${email} ${password}`, 'login');

    // Check if user exists
    const user = await this.userRepository.findOne({where: { email }, relations: ['roles', 'status']});
    if (!user) throw new HttpException('Invalid username or password.', HttpStatus.BAD_REQUEST);
    Logger.log(`User exists ${JSON.stringify(user)}`, 'login');

    // Check status
    Logger.log(`Checking status... ${user.status.name}`, 'login');
    switch (user.status.name) {
      case 'CREATED':
        throw new HttpException('Account not activated.', HttpStatus.UNAUTHORIZED);
      case 'BLOCKED':
        throw new HttpException('Account locked by administrator.', 423);
      case 'LOCKED':
        await this.checkLockStatus(user);
    }
    Logger.log(`User active`, 'login');

    // Password is unavailable via ORM methods
    const tmpUser = await this.userRepository
      .createQueryBuilder('user')
      .select('user.password')
      .where({ email })
      .getOne();
    const hash = tmpUser.password;
    Logger.log(`Comparing ${password} ${hash}`, 'login');
    const passwordsMatch = await bcrypt.compare(password, hash);
    Logger.log(`Compared ${passwordsMatch}`, 'login');

    if (!passwordsMatch) {
      Logger.log(`Password invalid`, 'login');

      user.loginAttempts += 1;
      await this.userRepository.save(user);
      if (user.loginAttempts >= this.loginAttemptsLimit) {
        Logger.log(`Failed login limit exceeded ${user.loginAttempts}`, 'login');
        const now = new Date().getTime();
        user.lockedUntil = new Date(now + this.lockTime).toISOString();
        user.status = await this.statusRepository.findOne({where: {name: 'LOCKED'}});
        Logger.log(`User locked ${user}`, 'login');
        await this.userRepository.save(user);
      }

      throw new HttpException('Invalid username or password.', HttpStatus.BAD_REQUEST);
    }

    // Create token
    const token = this.createSessionToken(user);
    Logger.log(`Credentials valid, created token ${token}`, 'login');

    // Return token
    return { token };
  }

  /**
   * Create new user account (used by users)
   * @param data
   */
  async register(data: RegisterDto): Promise<RegisterResponse> {

    // Check if user exists
    const { email } = data;
    let user = await this.userRepository.findOne({where: { email }});
    if (user) throw new HttpException('User already exists', HttpStatus.CONFLICT);

    // Get default status (CREATED)
    const status = await this.statusRepository.findOne({where: { name: 'CREATED' }});

    // Create user and set default status
    user = await this.userRepository.create(data);
    user.status = status;

    // Assign user role
    const role = await this.roleRepository.findOne({where: { name: 'USER'}});
    (user.roles) ? user.roles.push(role) : user.roles = [role];

    // Send email and save user to db
    await this.sendActivationEmail(user.email);
    await this.userRepository.save(user);

    return plainToClass(RegisterResponse, user, {strategy: 'excludeAll'});
  }

  /**
   * Use email token to activate the account
   * @param token
   */
  async activate(token: string): Promise<ApiSuccessDto> {
    const payload = this.validateToken(token, 'activation');

    // Add email confirmed event
    Logger.log(`Valid token ${JSON.stringify(payload)}`, 'activate');
    const user = await this.userRepository.findOne({where: {email: payload['email']}});

    // Change user status
    user.status = await this.statusRepository.findOne({where: { name: 'ACTIVE' }});
    await this.userRepository.save(user);
    Logger.log(`Updated user and added event ${user}`, 'activate');

    return {
      success: 200,
      message: 'Account activated.',
    };
  }

  /**
   * Send email with password reset token
   */
  async recoverAccount(email: string): Promise<ApiSuccessDto> {
    Logger.log(`Account recovery request from ${email}`, 'recoverAccount');

    const user = await this.userRepository.findOne({where: {email}});
    if (!user) return{ success: 200, message: 'Request received.' };

    this.sendRecoveryEmail(email).then();

    return {
      success: 200,
      message: 'Request received.',
    };
  }

  /**
   * Method for resending recovery emails for existing users.
   * @param email
   */
  async resendRecovery(email: string) {
    const user = await this.userRepository.findOne({where: {email}});
    if (!user) return HttpStatus.OK;
    this.sendRecoveryEmail(email).then();
  }

  /**
   * Method for resending activation emails for existing users.
   * @param email
   */
  async resendActivation(email: string) {
    const user = await this.userRepository.findOne({where: {email}, relations: ['status']});
    if (!user) return HttpStatus.OK;
    if (user.status.name !== 'CREATED') return HttpStatus.OK;
    this.sendActivationEmail(email).then();
  }

  /**
   * Use password reset token to change password
   */
  async resetPassword({token, password}) {

    // Get user from token
    const payload = this.validateToken(token, 'recovery');
    const user = await this.userRepository.findOne({where: {email: payload.email}});
    if (!user) return HttpStatus.BAD_REQUEST;

    // Update user password
    user.password = password;
    await this.userRepository.save(user);

    // Add log and inform user
    this.sendPasswordChangedEmail(user.email).then();
  }

  /* ---------------------------------- */

  /**
   * Check if account can be safely unlocked
   * @param user
   */
  async checkLockStatus(user: User) {

    const now = new Date().getTime();
    const lockTime = new Date(user.lockedUntil).getTime();
    const canUnlock = now > lockTime;

    if (canUnlock) {
      user.lockedUntil = undefined;
      user.loginAttempts = 0;
      user.status = await this.statusRepository.findOne({where: {name: 'ACTIVE'}});
      await this.userRepository.save(user);
    } else {
      throw new HttpException('Account locked by system.', HttpStatus.TOO_MANY_REQUESTS);
    }

  }

  /**
   * Simple wrapper for creating auth token after login
   * @param user
   */
  createSessionToken(user: User) {
    const expiresIn = this.sessionDuration;

    const token = {
      type: 'access',
      email: user.email,
    };

    const roles: string[] = user.roles.map(role => role.name);
    if (roles.indexOf('ADMIN') >= 0) token['admin'] = true;

    return jwt.sign(
      token,
      Configuration.sessionSecret,
      { expiresIn, algorithm: 'HS512' },
    );
  }

  /**
   * Send welcome email with email confirmation token
   * @param email
   */
  async sendActivationEmail(email: string) {
    const expiresIn = 48 * 60 * 60; // 48h
    Logger.log(`Sending activation email to ${email}`, 'sendActivationEmail');

    // Create activation token
    const activationToken = jwt.sign({
      type: 'activation',
      email: email,
    }, Configuration.tokenSecret, { expiresIn, algorithm: 'HS512' });

    const websiteLink = Configuration.website;
    const actionLink = `${websiteLink}activate/${activationToken}`;

    // Send email from template
    await this.mailerService.sendMail({
      to: email,
      subject: 'Viewer | Email confirmation',
      template: 'welcome', // The ``.hbs` extension is appended automatically.
      context: {  // Data to be sent to template engine.
        email: email,
        websiteLink: websiteLink,
        actionLink: actionLink,
      },
    });

  }

  async sendRecoveryEmail(email: string) {
    const expiresIn = 4 * 60 * 60; // 4h

    // Create activation token
    const activationToken = jwt.sign({
      type: 'recovery',
      email: email,
    }, Configuration.tokenSecret, { expiresIn, algorithm: 'HS512' });

    const websiteLink = Configuration.website;
    const actionLink = `${websiteLink}reset-password/${activationToken}`;

    // Send email from template
    await this.mailerService.sendMail({
      to: email,
      subject: 'Viewer | Account recovery',
      template: 'recovery',
      context: {
        email: email,
        websiteLink: websiteLink,
        actionLink: actionLink,
      },
    });

  }

  async sendPasswordChangedEmail(email) {
    const websiteLink = Configuration.website;

    // Send email from template
    await this.mailerService.sendMail({
      to: email,
      subject: 'Viewer | Password changed',
      template: 'password-changed',
      context: {
        websiteLink: websiteLink,
      },
    });
  }

  validateToken(token, type?: string) {
    let payload;
    const validationOptions = {
      algorithms: ['HS512'],
    };

    try {
      payload = jwt.verify(token, Configuration.tokenSecret, validationOptions);
    } catch (e) {
      throw new HttpException(`Invalid token: ${e.message}`, HttpStatus.BAD_REQUEST);
    }

    // Check type
    if (payload.type !== type) throw new HttpException(`Invalid token type`, HttpStatus.BAD_REQUEST);

    return payload;
  }

}
