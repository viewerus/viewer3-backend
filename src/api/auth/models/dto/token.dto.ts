import { IsNotEmpty } from 'class-validator';
import { ApiModelProperty } from '@nestjs/swagger';
import { Exclude, Expose } from 'class-transformer';

@Exclude()
export class TokenDto {
  @IsNotEmpty()
  @ApiModelProperty()
  @Expose()
  token: string;
}
