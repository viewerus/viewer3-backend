import { IsNotEmpty } from 'class-validator';
import { ApiModelPropertyOptional } from '@nestjs/swagger';
import { Exclude, Expose } from 'class-transformer';

@Exclude()
export class RegisterDto {
  @IsNotEmpty()
  @ApiModelPropertyOptional()
  @Expose()
  email: string;
}
