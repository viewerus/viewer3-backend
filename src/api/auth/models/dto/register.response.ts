import { ApiModelProperty } from '@nestjs/swagger';
import { Exclude, Expose } from 'class-transformer';

@Exclude()
export class RegisterResponse {

  @ApiModelProperty()
  @Expose()
  id: string;

  @ApiModelProperty()
  @Expose()
  email: string;

  @ApiModelProperty()
  @Expose()
  createdAt: string;
}
