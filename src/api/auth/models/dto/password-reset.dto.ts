import { IsNotEmpty, MaxLength, MinLength } from 'class-validator';
import { ApiModelProperty } from '@nestjs/swagger';
import { Exclude, Expose } from 'class-transformer';

@Exclude()
export class PasswordResetDto {
  @IsNotEmpty()
  @ApiModelProperty()
  @Expose()
  token: string;

  @IsNotEmpty()
  @MinLength(8)
  @MaxLength(60)
  // @MixedCase()
  // @IncludesCharacter(['!', '@', '#', '$', '%', '^', '&', '*', '?', '_', '~'])
  @ApiModelProperty()
  @Expose()
  password: string;
}
