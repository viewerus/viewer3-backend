import { IsEmail, IsNotEmpty, MaxLength, MinLength } from 'class-validator';
import { ApiModelProperty } from '@nestjs/swagger';
import { Exclude, Expose } from 'class-transformer';
import { IncludesCharacter, MixedCase } from '../../../../shared/decorators/validators';

@Exclude()
export class RegisterDto {
  @IsNotEmpty()
  @IsEmail()
  @MinLength(3)
  @MaxLength(60)
  @ApiModelProperty()
  @Expose()
  email: string;

  @IsNotEmpty()
  @MinLength(8)
  @MaxLength(60)
  // @MixedCase()
  // @IncludesCharacter(['!', '@', '#', '$', '%', '^', '&', '*', '?', '_', '~'])
  @ApiModelProperty()
  @Expose()
  password: string;
}
