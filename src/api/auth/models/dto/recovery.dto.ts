import { IsEmail, IsNotEmpty, MaxLength, MinLength } from 'class-validator';
import { ApiModelProperty } from '@nestjs/swagger';
import { Exclude, Expose } from 'class-transformer';

@Exclude()
export class RecoveryDto {
  @IsNotEmpty()
  @IsEmail()
  @MinLength(3)
  @MaxLength(60)
  @ApiModelProperty()
  @Expose()
  email: string;
}
