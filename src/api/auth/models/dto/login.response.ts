import { Exclude, Expose } from 'class-transformer';
import { ApiModelProperty } from '@nestjs/swagger';
import { UserDto } from '../../../user/models/dto/user.dto';

@Exclude()
export class LoginResponse {

  @ApiModelProperty()
  @Expose()
  expires: string;

  @ApiModelProperty()
  @Expose()
  token: any;

  @ApiModelProperty()
  @Expose()
  user: UserDto;
}
