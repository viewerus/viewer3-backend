import { Body, Controller, Delete, Get, Logger, Param, Patch, Post, Put, Req, UseGuards } from '@nestjs/common';
import { ApiUseTags, ApiOperation, ApiCreatedResponse, ApiBadRequestResponse, ApiNotFoundResponse, ApiOkResponse } from '@nestjs/swagger';
import { CreateUserDto } from './models/dto/create-user.dto';
import { UserDto } from './models/dto/user.dto';
import { ApiExceptionDto } from '../../shared/models/api-exception.dto';
import { User } from './models/entities/user.entity';
import { UserService } from './user.service';
import { ApiSuccessDto } from '../../shared/models/api-success.dto';
import { AuthGuard } from '@nestjs/passport';

@Controller('users')
@ApiUseTags('User')
export class UserController {

  constructor( private readonly userService: UserService ) {}

  @Get()
  @ApiOkResponse({ type: UserDto, isArray: true })
  @ApiOperation({
    title: 'Get all registered users (for admin)',
    operationId: 'User_GetAll',
  })
  async getAllUsers(): Promise<UserDto[]> {
    return await this.userService.getAll();
  }

  @Get('me')
  @UseGuards(AuthGuard())
  @ApiOkResponse({ type: UserDto })
  @ApiOperation({
    title: 'Get user from current session',
    operationId: 'User_GetMe',
  })
  async getUserBySession(@Req() req): Promise<UserDto> {
    Logger.log(`Called with ${JSON.stringify(req.user)}`, 'getUserBySession');
    return this.userService.getByEmail(req.user.email);
  }

  @Get(':id')
  @ApiOkResponse({ type: UserDto })
  @ApiNotFoundResponse({ type: ApiExceptionDto })
  @ApiOperation({
    title: 'Get user by id',
    operationId: 'User_Get',
  })
  async getUserById(@Param('id') id: string): Promise<UserDto> {
    return this.userService.get(id);
  }

  @Patch(':id')
  @ApiOkResponse({ type: UserDto })
  @ApiNotFoundResponse({ type: ApiExceptionDto })
  @ApiOperation({
    title: 'Update all or some of user\'s properties',
    operationId: 'User_Update',
  })
  async updateUser(
    @Param('id') id: string,
    @Body() data: CreateUserDto,
  ): Promise<UserDto> {
    return this.userService.update(id, data);
  }

  @Delete(':id')
  @ApiOkResponse({ type: {} })
  @ApiNotFoundResponse({ type: ApiExceptionDto })
  @ApiOperation({
    title: 'Remove user from database',
    operationId: 'User_Delete',
  })
  async deleteUser(@Param('id') id: string): Promise<ApiSuccessDto> {
    return this.userService.destroy(id);
  }

  @Post(':id/block')
  @ApiOkResponse({ type: UserDto })
  @ApiNotFoundResponse({ type: ApiExceptionDto })
  @ApiOperation({
    title: 'Lock user\'s account (for admin)',
    operationId: 'User_Lock',
  })
  async blockUser(@Param('id') id: string): Promise<ApiSuccessDto> {
    return this.userService.lock(id);
  }

  @Post(':id/unblock')
  @ApiOkResponse({ type: UserDto })
  @ApiNotFoundResponse({ type: ApiExceptionDto })
  @ApiOperation({
    title: 'Unlock user\'s account (for admin)',
    operationId: 'User_Unlock',
  })
  async unblockUser(@Param('id') id: string): Promise<ApiSuccessDto> {
    return this.userService.unlock(id);
  }

}
