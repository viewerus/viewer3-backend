import { ApiModelPropertyOptional } from '@nestjs/swagger';
import { Exclude, Expose } from 'class-transformer';

@Exclude()
export class UserDto {

  @ApiModelPropertyOptional()
  @Expose()
  email: string;

  @ApiModelPropertyOptional()
  @Expose()
  createdAt: string;

  @ApiModelPropertyOptional()
  @Expose()
  status: string;

}
