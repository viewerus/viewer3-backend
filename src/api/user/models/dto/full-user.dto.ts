import { ApiModelPropertyOptional } from '@nestjs/swagger';
import { Exclude, Expose } from 'class-transformer';
import { Status } from '../entities/status.entity';
import { Role } from '../entities/role.entity';

@Exclude()
export class FullUserDto {

  @ApiModelPropertyOptional()
  @Expose()
  email: string;

  @ApiModelPropertyOptional()
  @Expose()
  createdAt: string;

  @ApiModelPropertyOptional()
  @Expose()
  lastLogin: string;

  @ApiModelPropertyOptional()
  @Expose()
  status: Status;

  @ApiModelPropertyOptional()
  @Expose()
  roles: Role[];

}
