import { IsEmail, IsNotEmpty, MaxLength, MinLength } from 'class-validator';
import { ApiModelPropertyOptional } from '@nestjs/swagger';
import { Exclude, Expose } from 'class-transformer';

@Exclude()
export class CreateUserDto {
  @IsNotEmpty()
  @IsEmail()
  @MinLength(3)
  @MaxLength(64)
  @ApiModelPropertyOptional()
  @Expose()
  email: string;

  @IsNotEmpty()
  @MinLength(8)
  @MaxLength(64)
  @ApiModelPropertyOptional()
  @Expose()
  password: string;

}
