import { Column, Entity, Index, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Status {

  @PrimaryGeneratedColumn()
  id: number;

  @Column({type: 'varchar', length: 60})
  @Index({ unique: true })
  name: string;

  @Column()
  description: string;

}
