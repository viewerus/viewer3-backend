import { Column, Entity, Unique, PrimaryGeneratedColumn, Index } from 'typeorm';

@Entity()
export class Role {

  @PrimaryGeneratedColumn()
  id: number;

  @Column({type: 'varchar', length: 60})
  @Index({ unique: true })
  name: string;

  @Column()
  description: string;

}
