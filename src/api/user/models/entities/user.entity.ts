import { AfterLoad, BeforeInsert, BeforeUpdate, Column, Entity, Index, JoinTable, ManyToMany, ManyToOne, OneToMany } from 'typeorm';
import { BaseEntity } from '../../../../base/base.entity';
import { Status } from './status.entity';
import { Role } from './role.entity';
import { Project } from '../../../project/models/project.entity';
import * as bcrypt from 'bcryptjs';

@Entity()
export class User extends BaseEntity {

  @Column('varchar', { length: 60 })
  @Index({ unique: true })
  email: string;

  @Column('varchar', { length: 60, select: false})
  password: string;

  @Column('smallint', {default: 0})
  loginAttempts: number;

  @Column('timestamptz', {nullable: true})
  lockedUntil: Date | string;

  @ManyToOne(type => Status, model => model.id)
  status: Status;

  @ManyToMany(type => Role)
  @JoinTable()
  roles: Role[];

  @OneToMany(type => Project, model => model.author)
  projects: Project[];

  private tempPassword: string;

  @AfterLoad()
  private loadTempPassword(): void {
    this.tempPassword = this.password;
  }

  @BeforeInsert()
  @BeforeUpdate()
  private encryptPassword(): void {
    if (this.tempPassword === this.password) return;
    this.password = bcrypt.hashSync(this.password, 10);
  }

}
