import { HttpException, HttpStatus, Injectable, Logger } from '@nestjs/common';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from './models/entities/user.entity';
import { CreateUserDto } from './models/dto/create-user.dto';
import { UserDto } from './models/dto/user.dto';
import { plainToClass } from 'class-transformer';
import { ApiSuccessDto } from '../../shared/models/api-success.dto';
import { Status } from './models/entities/status.entity';

@Injectable()
export class UserService {

  constructor(
    @InjectRepository(User)
    private userRepository: Repository<User>,
    @InjectRepository(Status)
    private statusRepository: Repository<Status>,
  ) {}

  async get(id: string): Promise<UserDto> {
    const user = await this.userRepository.findOne(id);
    return plainToClass(UserDto, user, {strategy: 'excludeAll'});
  }

  async getAll(): Promise<UserDto[]> {
    const users = await this.userRepository.find({relations: ['status', 'roles']});
    Logger.log(`users ${JSON.stringify(users)}`, 'Users - getAll');

    const response = [];
    for (const user of users) {
      const roles = user.roles.map(role => role.name);
      const item = {
        id: user.id,
        email: user.email,
        status: user.status.name,
        created: user.createdAt,
        roles,
      };
      response.push(item);
    }

    Logger.log(`Response ${JSON.stringify(response)}`, 'Users - getAll');
    return response;
  }

  async create(data: CreateUserDto): Promise<UserDto> {
    const { email } = data;
    let user = await this.userRepository.findOne({where: { email }});

    if (user) {
      throw new HttpException('User already exists', HttpStatus.BAD_REQUEST);
    }

    user = await this.userRepository.create(data);
    await this.userRepository.save(user);
    return plainToClass(UserDto, user, {strategy: 'excludeAll'});
  }

  async update(id: string, data: Partial<CreateUserDto>): Promise<UserDto> {
    const user = await this.userRepository.findOne({where: { id }});

    if (!user) {
      throw new HttpException('Not found', HttpStatus.NOT_FOUND);
    }

    const userData = plainToClass(CreateUserDto, data, {strategy: 'excludeAll'});

    await this.userRepository.update({ id }, userData);
    const updatedUser = await this.userRepository.save(user);

    return plainToClass(UserDto, updatedUser, {strategy: 'excludeAll'});
  }

  async destroy(id: string): Promise<ApiSuccessDto> {
    const user = await this.userRepository.findOne({where: { id }});

    if (!user) {
      throw new HttpException('Not found', HttpStatus.NOT_FOUND);
    }

    await this.userRepository.delete(id);

    return {success: 200, message: `Resource ${id} destroyed.`};
  }

  async lock(id: string): Promise<ApiSuccessDto> {
    const user = await this.userRepository.findOne({where: { id }});

    if (!user) {
      throw new HttpException('Not found', HttpStatus.NOT_FOUND);
    }

    user.status = await this.statusRepository.findOne({where: {name: 'BLOCKED'}});
    delete user.updatedAt;

    await this.userRepository.update(id, user);

    return {success: 200, message: `User ${id} blocked.`};
  }

  async unlock(id: string): Promise<ApiSuccessDto> {
    const user = await this.userRepository.findOne({where: { id }});

    if (!user) {
      throw new HttpException('Not found', HttpStatus.NOT_FOUND);
    }

    user.status = await this.statusRepository.findOne({where: {name: 'ACTIVE'}});
    delete user.updatedAt;

    await this.userRepository.update(id, user);

    return {success: 200, message: `User ${id} blocked.`};
  }

  async getByEmail(email: string) {
    const user = await this.userRepository.findOne({where: {email}});

    if (!user) {
      throw new HttpException('Not found', HttpStatus.NOT_FOUND);
    }

    return plainToClass(UserDto, user, {strategy: 'excludeAll'});
  }

}
