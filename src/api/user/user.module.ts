import { Module } from '@nestjs/common';
import { UserController } from './user.controller';
import { UserService } from './user.service';
import { User } from './models/entities/user.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Role } from './models/entities/role.entity';
import { Status } from './models/entities/status.entity';
import { PassportModule } from '@nestjs/passport';

@Module({
  imports: [
    TypeOrmModule.forFeature([Role, User, Status]),
    PassportModule.register({defaultStrategy: 'jwt'}),
  ],
  controllers: [UserController],
  providers: [UserService],
  exports: [UserService],
})
export class UserModule {}
