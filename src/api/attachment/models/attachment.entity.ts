import { Column, CreateDateColumn, Entity, Index, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { Project } from '../../project/models/project.entity';
import { AttachmentType } from './attachment-type.entity';

@Entity()
export class Attachment {

  @PrimaryGeneratedColumn('uuid')
  id: string;

  @CreateDateColumn()
  createdAt?: string;

  @Column({type: 'varchar', length: 60})
  @Index()
  name: string;

  @ManyToOne(type => AttachmentType, model => model.name)
  type: AttachmentType;

  @ManyToOne(type => Project, model => model.files, {onDelete: 'CASCADE'})
  project: Project;

}
