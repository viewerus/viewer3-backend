import { Exclude, Expose } from 'class-transformer';
import { IsNotEmpty } from 'class-validator';
import { ApiModelProperty } from '@nestjs/swagger';

@Exclude()
export class AttachmentCreateDto {

  @IsNotEmpty()
  @ApiModelProperty()
  @Expose()
  name: string;

  @IsNotEmpty()
  @ApiModelProperty()
  @Expose()
  projectId: string;

  @IsNotEmpty()
  @ApiModelProperty()
  @Expose()
  file: string;

}
