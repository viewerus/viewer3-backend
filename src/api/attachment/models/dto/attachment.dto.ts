import { Exclude, Expose } from 'class-transformer';
import { IsNotEmpty } from 'class-validator';
import { ApiModelProperty } from '@nestjs/swagger';

@Exclude()
export class AttachmentDto {

  @IsNotEmpty()
  @ApiModelProperty()
  @Expose()
  id: string;

  @IsNotEmpty()
  @ApiModelProperty()
  @Expose()
  name: string;

  @IsNotEmpty()
  @ApiModelProperty()
  @Expose()
  type: string;

  @IsNotEmpty()
  @ApiModelProperty()
  @Expose()
  project: string;

}
