import { ForbiddenException, Injectable, Logger, NotFoundException } from '@nestjs/common';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { ApiSuccessDto } from '../../shared/models/api-success.dto';
import { Attachment } from './models/attachment.entity';
import { Configuration } from '../../shared/configuration/configuration';

@Injectable()
export class AttachmentService {

  constructor(
    @InjectRepository(Attachment)
    private readonly repository: Repository<Attachment>,
  ) {}

  public async get(id: string, email: string): Promise<any> {
    const item = await this.repository.createQueryBuilder('file')
      .where('file.id = :id')
      .innerJoinAndSelect('file.type', 'type')
      .innerJoinAndSelect('file.project', 'project')
      .innerJoinAndSelect('project.author', 'user')
      .setParameter('id', id)
      .getOne();

    // Check if exists
    if (!item) throw new NotFoundException();

    // Check permission
    if (item.project.author.email !== email) throw new ForbiddenException();

    // Read associated file
    const path = Configuration.basePath() + '/upload/' + item.name;

    Logger.log(`Getting... ${path}`, 'GetFile');
    return require(path);

  }

  public async delete(id: string, email: string): Promise<ApiSuccessDto> {
    return null;
  }

}
