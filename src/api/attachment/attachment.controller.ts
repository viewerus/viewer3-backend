import {
  Controller,
  Delete,
  Get, InternalServerErrorException,
  Param,
  Req,
  UseGuards,
} from '@nestjs/common';
import { ApiUseTags, ApiBearerAuth, ApiOperation, ApiNotFoundResponse, ApiOkResponse,
  ApiImplicitParam } from '@nestjs/swagger';
import { ApiExceptionDto } from '../../shared/models/api-exception.dto';
import { ApiSuccessDto } from '../../shared/models/api-success.dto';
import { AuthGuard } from '@nestjs/passport';
import { AttachmentService } from './attachment.service';

@Controller('files')
@ApiUseTags('Files')
export class AttachmentController {

  constructor(private readonly service: AttachmentService) {}

  @Get(':id')
  @UseGuards(AuthGuard('jwt'))
  @ApiBearerAuth()
  @ApiOkResponse({ type: ApiSuccessDto })
  @ApiNotFoundResponse({ type: ApiExceptionDto })
  @ApiOperation({
    title: 'Get content of a single file',
    operationId: 'Files_Get',
  })
  async get(@Param('id') id: string, @Req() req): Promise<any> {
    return this.service.get(id, req.user.email);
  }

  @Delete(':id')
  @UseGuards(AuthGuard('jwt'))
  @ApiBearerAuth()
  @ApiImplicitParam({
    name: 'id',
    required: true,
    description: 'Id of Object',
    type: String,
  })
  @ApiOkResponse({ type: ApiSuccessDto })
  @ApiOperation({
    title: 'Remove file.',
    operationId: 'Files_Delete' })
  public async delete(@Param('id') id: string, @Req() req): Promise<any> {
    try {
      return this.service.delete(id, req.user.email);
    } catch (e) {
      throw new InternalServerErrorException(e);
    }
  }

}
