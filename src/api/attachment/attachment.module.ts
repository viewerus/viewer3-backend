import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { PassportModule } from '@nestjs/passport';
import { Attachment } from './models/attachment.entity';
import { AttachmentController } from './attachment.controller';
import { AttachmentService } from './attachment.service';

@Module({
  imports: [
    TypeOrmModule.forFeature([Attachment]),
    PassportModule,
  ],
  providers: [AttachmentService],
  controllers: [AttachmentController],
})
export class AttachmentModule {}
