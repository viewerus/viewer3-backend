import { TicketDto } from './models/dto/ticket.dto';
import { Inject, UnauthorizedException } from '@nestjs/common';
import { MailerService } from '@nest-modules/mailer';
import { TicketCreateDto } from './models/dto/ticket-create.dto';
import { Configuration } from '../../shared/configuration/configuration';
import { getRepository } from 'typeorm';
import { User } from '../user/models/entities/user.entity';

export class TicketService {

  constructor(
    @Inject('MailerService')
    private readonly mailerService: MailerService,
  ) {}

  async create(data: TicketCreateDto, email: string): Promise<any> {
    // TODO add proper ticketing system (ticket, message)

    // Get user by email - if no user or user !== data.email 403
    const user = await getRepository(User).findOne({where: {email}});
    if (!user || user.email !== email) throw new UnauthorizedException();

    let tag = '';

    switch (data.category) {
      case 'bug': tag = 'Bug report'; break;
      case 'feature': tag = 'Feature request'; break;
      default: tag = 'Contact request';
    }

    const subject = `Viewer | ${tag}: ${data.topic}`;

    await this.mailerService.sendMail({
      to: Configuration.adminEmail,
      from: email,
      subject: subject,
      template: 'contact', // The ``.hbs` extension is appended automatically.
      context: {  // Data to be sent to template engine.
        email: email,
        type: tag,
        message: data.message,
      },
    });

    return {
      success: 200,
      message: 'Email sent.',
    };
  }

  // TODO later design proper ticketing support system
  async get(id: string, email: string): Promise<TicketDto> {
    return null;
  }

  async getAll(email: string): Promise<TicketDto[]> {
    return null;
  }

  async close(data, email): Promise<TicketDto> {
    return null;
  }

}
