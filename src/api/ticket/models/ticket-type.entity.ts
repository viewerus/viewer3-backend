import { Column, Entity, Index, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class TicketType {

  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column({type: 'varchar', length: 60})
  @Index({ unique: true })
  name: string;

  @Column({type: 'varchar', length: 60})
  description: string;

}
