import { Exclude, Expose } from 'class-transformer';
import { IsNotEmpty, MaxLength } from 'class-validator';
import { ApiModelProperty } from '@nestjs/swagger';

@Exclude()
export class TicketCreateDto {

  @IsNotEmpty()
  @ApiModelProperty()
  @Expose()
  email: string;

  @IsNotEmpty()
  @ApiModelProperty()
  @MaxLength(100)
  @Expose()
  category: 'bug' | 'feature' | 'other';

  @IsNotEmpty()
  @ApiModelProperty()
  @Expose()
  topic: string;

  @IsNotEmpty()
  @ApiModelProperty()
  @Expose()
  message: string;

}
