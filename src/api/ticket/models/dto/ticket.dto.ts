import { IsNotEmpty } from 'class-validator';
import { Exclude, Expose } from 'class-transformer';
import { ApiModelProperty } from '@nestjs/swagger';
import { TicketType } from '../ticket-type.entity';
import { User } from '../../../user/models/entities/user.entity';

Exclude();
export class TicketDto {

  @IsNotEmpty()
  @ApiModelProperty()
  @Expose()
  id: string;

  @IsNotEmpty()
  @ApiModelProperty()
  @Expose()
  topic: string;

  @IsNotEmpty()
  @ApiModelProperty()
  @Expose()
  message: string;

  @IsNotEmpty()
  @ApiModelProperty()
  @Expose()
  type: TicketType;

  @IsNotEmpty()
  @ApiModelProperty()
  @Expose()
  user: User;

  @IsNotEmpty()
  @ApiModelProperty()
  @Expose()
  createdAt: string;

}
