import { Column, CreateDateColumn, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { TicketType } from './ticket-type.entity';
import { User } from '../../user/models/entities/user.entity';

@Entity()
export class Ticket {

  @PrimaryGeneratedColumn('uuid')
  id: string;

  @CreateDateColumn()
  createdAt?: string;

  @Column({type: 'varchar', length: 100})
  topic: string;

  @Column({type: 'varchar'})
  message: string;

  @ManyToOne(type => TicketType, model => model.name)
  type: TicketType;

  @ManyToOne(type => User, model => model.id)
  user: User;
}
