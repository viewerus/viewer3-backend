import {
  Body,
  Controller,
  Delete,
  Get,
  InternalServerErrorException,
  Param,
  Post,
  Req,
  UseGuards,
} from '@nestjs/common';
import {
  ApiBadRequestResponse,
  ApiBearerAuth,
  ApiCreatedResponse,
  ApiImplicitBody,
  ApiImplicitParam,
  ApiNotFoundResponse,
  ApiOkResponse,
  ApiOperation,
  ApiUseTags,
} from '@nestjs/swagger';
import { ApiExceptionDto } from '../../shared/models/api-exception.dto';
import { ProjectDto } from '../project/models/dto/project.dto';
import { ApiSuccessDto } from '../../shared/models/api-success.dto';
import { TicketService } from './ticket.service';
import { Ticket } from './models/ticket.entity';
import { AuthGuard } from '@nestjs/passport';
import { TicketDto } from './models/dto/ticket.dto';
import { TicketCreateDto } from './models/dto/ticket-create.dto';

@Controller('tickets')
@ApiUseTags('Tickets')
export class TicketController {

  constructor(private readonly service: TicketService) {}

  @Post()
  @UseGuards(AuthGuard('jwt'))
  @ApiBearerAuth()
  @ApiImplicitBody({
    name: Ticket.name,
    type: TicketCreateDto,
    description: 'Data for resource creation.',
    required: true,
    isArray: false,
  })
  @ApiCreatedResponse({ type: ApiSuccessDto })
  @ApiBadRequestResponse({ type: ApiExceptionDto })
  @ApiOperation({
    title: 'Create new ticket',
    operationId: 'Ticket_Create',
  })
  async create(@Body() data: TicketCreateDto, @Req() req): Promise<any> {
    return await this.service.create(data, req.user.email);
  }

  /*
  @Get(':id')
  @UseGuards(AuthGuard('jwt'))
  @ApiBearerAuth()
  @ApiOkResponse({ type: TicketDto })
  @ApiNotFoundResponse({ type: ApiExceptionDto })
  @ApiOperation({
    title: 'Get details of project',
    operationId: 'Project_Get',
  })
  async get(@Param('id') id: string, @Req() req): Promise<TicketDto> {
    return this.service.get(id, req.user.email);
  }

  @Get('')
  @UseGuards(AuthGuard('jwt'))
  @ApiBearerAuth()
  @ApiOkResponse({ type: TicketDto, isArray: true })
  @ApiNotFoundResponse({ type: ApiExceptionDto })
  @ApiOperation({
    title: 'Get details of project',
    operationId: 'Project_Get',
  })
  async getAll(string, @Req() req): Promise<TicketDto[]> {
    return this.service.getAll(req.user.email);
  }

  @Delete(':id')
  @UseGuards(AuthGuard('jwt'))
  @ApiBearerAuth()
  @ApiImplicitParam({
    name: 'id',
    required: true,
    description: 'Id of Object',
    type: String,
  })
  @ApiOkResponse({ type: ApiSuccessDto })
  @ApiOperation({
    title: 'Remove project',
    operationId: 'Project_Delete' })
  public async close(@Param('id') id: string, @Req() req): Promise<any> {
    try {
      return this.service.close(id, req.user.email);
    } catch (e) {
      throw new InternalServerErrorException(e);
    }
  }
  */
}
