import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { PassportModule } from '@nestjs/passport';
import { Ticket } from './models/ticket.entity';
import { TicketService } from './ticket.service';
import { TicketController } from './ticket.controller';

@Module({
  imports: [
    // TypeOrmModule.forFeature([Ticket]),
    PassportModule,
  ],
  providers: [TicketService],
  controllers: [TicketController],
})
export class TicketModule {}
