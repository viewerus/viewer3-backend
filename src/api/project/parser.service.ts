/* tslint:disable:no-console */
import * as readline from 'readline';
import * as fs from 'fs';
import { Injectable, Logger } from '@nestjs/common';
import { getRepository } from 'typeorm';
import { Project } from './models/project.entity';

type Vec3D = [number, number, number, number?];
type Model = Group[];

interface Group {
  name: string;
  selectable: boolean;
  size: number;
  color: string;
  opacity: number;
  parent: string;
  children: Group[] & Vec3D[];
}

@Injectable()
export class ParserService {
  public groupsTable: {[x: string]: Group};
  public currentGroup: Group;
  public groups: Group[];
  public defaults: Group;
  public output: string;

  public reader: readline.Interface;

  constructor() {
    this.groupsTable = {};
    this.groups = [];
    this.defaults = {
      name: 'defaults',
      parent: null,
      size: 1,
      color: '#ffffff',
      opacity: 1,
      selectable: true,
      children: [],
    };
  }

  async parseCsvToModel(inputPath: string, outputPath: string, projectId: string) {
    this.groupsTable = {};
    this.groups = [];
    this.output = outputPath;

    this.initFileReader(inputPath);
    this.createDefaultGroup();
    this.createGroupsFromFile(projectId);
  }

  initFileReader(fileName: string) {
    this.reader = readline.createInterface({
      input: fs.createReadStream(fileName),
      crlfDelay: Infinity,
    });
  }

  createDefaultGroup() {
    const group = {
      name: 'defaults',
      parent: null,
      size: 1,
      color: '#ffffff',
      opacity: 1,
      selectable: true,
      children: [],
    };

    this.groupsTable[group.name] = group;
    this.currentGroup = group;
  }

  createGroupsFromFile(id: string) {

    this.reader.on('line', (line) => {
      // Logger.log(`Line from file: ${line}`, 'Parser');

      // Get columns (replace empty with nulls)
      const columns = line
        .split(',')
        .map(column => (['', '*'].includes(column)) ? null : column);

      // Logger.log(`Columns: ${columns}`, 'Parser');

      if (columns[0] === 'group') {
        this.createGroup(columns);
      }

      if (!isNaN(parseFloat(columns[0]))) {
        this.addPoint(columns);
      }

      return;
    });

    this.reader.on('close', () => {
      this.initDefaults();
      this.makeHierarchy();
      this.assignValues(this.groups);
      this.saveModel();
      this.openProject(id);
      // Logger.log(`Assigned values ${JSON.stringify(this.groups)}`, 'Parser');
    });

  }

  createGroup(columns) {

    // Get correct parent
    let parent = columns[2] || 'defaults';

    if (columns[1] === 'defaults') {
      parent = null;
    }

    const group: Group = {
      name: columns[1],
      parent: parent,
      size: ( isNaN(parseFloat(columns[3])) ) ? null : parseFloat(columns[3]),
      color: columns[4],
      opacity: ( isNaN(parseFloat(columns[5])) ) ? null : parseFloat(columns[5]),
      selectable: (columns[6] === null) ? null : (['true', 'TRUE', '1'].includes(columns[6])),
      children: [],
    };

    Logger.log(`Saving group: ${JSON.stringify(group)}`, 'Parser');

    this.groupsTable[group.name] = group;
    this.currentGroup = group;
  }

  addPoint(columns) {
    const x = parseFloat(columns[0]);
    const y = parseFloat(columns[1]);
    const z = parseFloat(columns[2]);
    const size = parseFloat(columns[3]);

    const array: Vec3D = (isNaN(size)) ? [x, y, z] : [x, y, z, size];
    this.currentGroup.children.push(array);
  }

  initDefaults() {
    this.groupsTable['defaults'] = (this.groupsTable['defaults'] === undefined)
      ? this.defaults
      : Object.assign(this.defaults, this.groupsTable['defaults']);

    this.groupsTable['defaults'].parent = null;
    Logger.log(`Created defaults ${JSON.stringify(this.groupsTable['defaults'])}`, 'Parser');
  }

  makeHierarchy() {
    Logger.log(` Creating hierarchy... ${JSON.stringify(Object.keys(this.groupsTable))}`, 'Parser');

    for (const groupName in this.groupsTable) {
      if (this.groupsTable.hasOwnProperty(groupName)) {

        const group = this.groupsTable[groupName];
        Logger.log(`Checking group ${group.name}`, 'Parser');

        if (!group.parent) {
          Logger.log('  No parent, adding to groups', 'Parser');

          this.groups.push(group);
          continue;
        }

        if (!this.groupsTable[group.parent]) {
          Logger.log('  Parent doesnt exist, creating', 'Parser');
          this.groupsTable[group.parent] = Object.assign(this.defaults, {name: group.parent, parent: 'defaults'});
        }

        Logger.log(`Assigning group ${this.groupsTable[group.parent]}`, 'Parser');

        this.groupsTable[group.parent].children.push(group);

      }
    }
    // Logger.log(`Created hierarchy ${JSON.stringify(this.groups)}`, 'Parser');

  }

  assignValues(groups) {
    for (const group of groups) {
      if (group instanceof Array) return;
      if (!group.parent) this.assignValues(group.children);
      // Logger.log(`Assigning group ${JSON.stringify(group)}`, 'Parser');

      const properties = ['size', 'color', 'opacity', 'selectable'];

      for (const property of properties) {
        if (group[property] != null) continue;
        group[property] = this.getAscendantValue(group, property);
      }

      this.assignValues(group.children);
    }
  }

  getAscendantValue(group, property): number | boolean {
    Logger.log(`Value ${group.name} ${property} ${this.groupsTable[group.parent][property]}`, 'Parser');
    if (group.parent && this.groupsTable[group.parent][property] != null) {
      return this.groupsTable[group.parent][property];
    }
    this.getAscendantValue(group.parent, property);
  }

  saveModel() {
    fs.writeFileSync(this.output, JSON.stringify(this.groups));
  }

  async openProject(id: string) {
    Logger.log(`Updating project ${id}`, 'Parser');

    const project = await getRepository(Project).findOne(id);
    project.ready = true;
    await getRepository(Project).save(project);
  }

}
