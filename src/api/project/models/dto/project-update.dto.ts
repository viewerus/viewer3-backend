import { Exclude, Expose } from 'class-transformer';
import { Allow, IsNotEmpty, MinLength } from 'class-validator';
import { ApiModelProperty } from '@nestjs/swagger';
import { Attachment } from '../../../attachment/models/attachment.entity';

@Exclude()
export class ProjectUpdateDto {

  @Allow()
  @ApiModelProperty()
  @Expose()
  name: string;

  @Allow()
  @ApiModelProperty()
  @Expose()
  description: string;

}
