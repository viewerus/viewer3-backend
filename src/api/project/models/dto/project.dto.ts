import { Exclude, Expose } from 'class-transformer';
import { Allow, IsNotEmpty } from 'class-validator';
import { ApiModelProperty } from '@nestjs/swagger';
import { Attachment } from '../../../attachment/models/attachment.entity';

@Exclude()
export class ProjectDto {

  @IsNotEmpty()
  @ApiModelProperty()
  @Expose()
  id: string;

  @IsNotEmpty()
  @ApiModelProperty()
  @Expose()
  name: string;

  @Allow()
  @ApiModelProperty()
  @Expose()
  description: string;

  @IsNotEmpty()
  @ApiModelProperty()
  @Expose()
  createdAt: string;

  @IsNotEmpty()
  @ApiModelProperty()
  @Expose()
  updatedAt: string;

  @IsNotEmpty()
  @ApiModelProperty()
  @Expose()
  author: string;

  @IsNotEmpty()
  @ApiModelProperty()
  @Expose()
  files: Attachment[];

}
