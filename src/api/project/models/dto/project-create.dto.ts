import { Exclude, Expose } from 'class-transformer';
import { Allow, IsNotEmpty } from 'class-validator';
import { ApiModelProperty } from '@nestjs/swagger';
import { Attachment } from '../../../attachment/models/attachment.entity';

@Exclude()
export class ProjectCreateDto {

  @IsNotEmpty()
  @ApiModelProperty()
  @Expose()
  name: string;

  @Allow()
  @ApiModelProperty()
  @Expose()
  description: string;

  @IsNotEmpty()
  @ApiModelProperty()
  @Expose()
  fileType: string;

}
