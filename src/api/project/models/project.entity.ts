import { Column, Entity, Index, JoinColumn, ManyToOne, OneToMany } from 'typeorm';
import { BaseEntity } from '../../../base/base.entity';
import { User } from '../../user/models/entities/user.entity';
import { Attachment } from '../../attachment/models/attachment.entity';

@Entity()
export class Project extends BaseEntity {

  @Column({type: 'varchar', length: 60})
  @Index()
  name: string;

  @Column({type: 'text', nullable: true})
  description: string;

  @Column({type: 'boolean'})
  ready: boolean;

  @ManyToOne(type => User, model => model.projects)
  author: User;

  @JoinColumn()
  @OneToMany(type => Attachment, model => model.project)
  files: Attachment[];

}
