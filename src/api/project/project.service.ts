import { BadRequestException, ForbiddenException, Injectable, Logger, NotFoundException, UnauthorizedException } from '@nestjs/common';
import { getRepository, Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { ApiSuccessDto } from '../../shared/models/api-success.dto';
import { Project } from './models/project.entity';
import { ProjectDto } from './models/dto/project.dto';
import { ProjectCreateDto } from './models/dto/project-create.dto';
import { plainToClass } from 'class-transformer';
import { User } from '../user/models/entities/user.entity';
import { Attachment } from '../attachment/models/attachment.entity';
import { AttachmentType } from '../attachment/models/attachment-type.entity';
import { ProjectUpdateDto } from './models/dto/project-update.dto';
import { ParserService } from './parser.service';
import { Configuration } from '../../shared/configuration/configuration';
import * as path from 'path';

@Injectable()
export class ProjectService {

  constructor(
    @InjectRepository(Project)
    private readonly repository: Repository<Project>,
    private readonly parser: ParserService,
  ) {}

  /**
   * Get all items belonging to the user.
   * @param email
   */
  public async getAll(email: string): Promise<any[]> {
    const items = await this.repository.createQueryBuilder('project')
      .innerJoinAndSelect('project.author', 'user', 'user.email = :email')
      .leftJoinAndSelect('project.files', 'attachment')
      .leftJoinAndSelect('attachment.type', 'type')
      .setParameter('email', email)
      .getMany();

    if (!items) throw new NotFoundException();

    Logger.log(`User projects: ${JSON.stringify(items)}`, 'Projects getAll');
    return items;
  }

  /**
   * Get user's item by id.
   * @param id
   * @param email
   */
  public async get(id: string, email: string): Promise<any> {
    const item = await this.repository.createQueryBuilder('project')
      .where('project.id = :id')
      .innerJoinAndSelect('project.author', 'user')
      .leftJoinAndSelect('project.files', 'attachment')
      .leftJoinAndSelect('attachment.type', 'type')
      .select('project.id')
      .addSelect('project.name')
      .addSelect('project.description')
      .addSelect('user.email')
      .addSelect('attachment')
      .addSelect('type.name')
      .setParameter('id', id)
      .getOne();

    if (!item) throw new NotFoundException();
    if (item.author.email !== email) throw new ForbiddenException();

    return item;
  }

  /**
   * Update existing item.
   * @param id
   * @param data
   * @param email
   */
  public async update(id: string, data: ProjectUpdateDto, email: string): Promise<ProjectDto> {
    const item = await this.repository.findOne(id, {relations: ['author']});

    if (!item) throw new NotFoundException();
    if (item.author.email !== email) throw new ForbiddenException();

    if (item.name && item.name.length === 0) throw new BadRequestException('Invalid (empty) name');

    data = plainToClass(ProjectUpdateDto, data, {strategy: 'excludeAll'});
    const newData = await this.repository.merge(item, data);
    delete newData['updatedAt'];
    delete newData['createdAt'];

    await this.repository.update({ id }, newData);
    const updated = await this.repository.findOne(id);

    return plainToClass(ProjectDto, updated, {strategy: 'excludeAll'});
  }

  /**
   * Remove existing item.
   * @param id
   * @param email
   */
  public async delete(id: string, email: string): Promise<ApiSuccessDto> {
    const item = await this.repository.findOne(id, {relations: ['author']});

    if (!item) throw new NotFoundException();
    if (item.author.email !== email) throw new ForbiddenException();

    await this.repository.delete(id);
    return {success: 200, message: 'Removed', data: {id}};
  }

  public async createFromModel(data: ProjectCreateDto, email: string, file: any): Promise<ProjectDto> {
    Logger.log(`Create: ${JSON.stringify(data)}`, 'Project');

    data['ready'] = true;
    const item = await this.repository.create(data);
    const user = await getRepository(User).findOne({where: {email}});

    if (!user) throw new UnauthorizedException();
    if (!item) throw new BadRequestException();

    const type = await getRepository(AttachmentType).findOne({
      where: { name: data.fileType.toUpperCase() },
    });

    const attachmentData = await getRepository(Attachment).create({
      name: file.filename,
      type: type,
    });

    const attachment = await getRepository(Attachment).save(attachmentData);

    item.files = [attachment];
    item.author = user;
    await this.repository.save(item);

    return plainToClass(ProjectDto, item, {strategy: 'excludeAll'});
  }

  public async createFromSpreadsheet(data: ProjectCreateDto, email: string, file: any): Promise<ProjectDto> {

    const ext = path.extname(file.filename);
    const outputName = file.filename.substr(0, file.filename.length - ext.length) + '.json';
    Logger.log(`${ext} ${file.filename}`, 'Parsing');
    const input = Configuration.basePath() + '/upload/tmp/' + file.filename;
    const output = Configuration.basePath() + '/upload/' + outputName;

    Logger.log(`Create: ${JSON.stringify(data)}`, 'Project');
    data['ready'] = false;
    const item = await this.repository.create(data);
    const user = await getRepository(User).findOne({where: {email}});

    if (!user) throw new UnauthorizedException();
    if (!item) throw new BadRequestException();

    const type = await getRepository(AttachmentType).findOne({
      where: { name: 'MODEL' },
    });

    const attachmentData = await getRepository(Attachment).create({
      name: outputName,
      type: type,
    });

    const attachment = await getRepository(Attachment).save(attachmentData);

    item.files = [attachment];
    item.author = user;
    await this.repository.save(item);

    Logger.log(`${input} ${output}`, 'Parsing...');
    this.parser.parseCsvToModel(input, output, item.id);

    return plainToClass(ProjectDto, item, {strategy: 'excludeAll'});
  }

  public async createFromImage(data: ProjectCreateDto, email: string, file: any): Promise<ProjectDto> {
    // this will not be implemented in this project.
    return null;
  }

}
