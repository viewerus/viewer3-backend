import { Module, MulterModule } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { PassportModule } from '@nestjs/passport';
import { Project } from './models/project.entity';
import { ProjectService } from './project.service';
import { ProjectController } from './project.controller';
import * as multer from 'multer';
import * as sanitize from 'sanitize-filename';
import { ParserService } from './parser.service';
import { Configuration } from '../../shared/configuration/configuration';
import { randomBytes } from 'crypto';

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, `${Configuration.basePath()}/upload`);
  },
  filename: (req, file, cb) => {
    const sanitized = sanitize(file.originalname);

    const name = sanitized.split('.')[0];
    const extension = sanitized.split('.')[1];

    const filename = randomBytes(12).toString('hex') + '_' + name.substr(0, 60 - extension.length) + '.' + extension;

    cb(null, filename);
  },
});

@Module({
  imports: [
    TypeOrmModule.forFeature([Project]),
    PassportModule,
    MulterModule.register({
      storage: storage,
      limits: {
        fileSize: 10 * 1024 * 1024, // 10 MB
        files: 1,
      },
    }),
  ],
  providers: [ProjectService, ParserService],
  controllers: [ProjectController],
})
export class ProjectModule {}
