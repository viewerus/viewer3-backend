import {
  BadRequestException,
  Body,
  Controller,
  Delete,
  FileInterceptor,
  Get,
  InternalServerErrorException,
  Logger,
  Param,
  Patch,
  Post,
  Req,
  UploadedFile,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import {
  ApiBadRequestResponse,
  ApiBearerAuth,
  ApiCreatedResponse,
  ApiImplicitBody,
  ApiImplicitParam,
  ApiNotFoundResponse,
  ApiOkResponse,
  ApiOperation,
  ApiUseTags,
} from '@nestjs/swagger';
import { AuthGuard } from '@nestjs/passport';
import { ApiExceptionDto } from '../../shared/models/api-exception.dto';
import { ApiSuccessDto } from '../../shared/models/api-success.dto';
import { ProjectCreateDto } from './models/dto/project-create.dto';
import { ProjectDto } from './models/dto/project.dto';
import { Project } from './models/project.entity';
import { ProjectService } from './project.service';
import { ProjectUpdateDto } from './models/dto/project-update.dto';
import * as path from 'path';
import * as multer from 'multer';
import { Configuration } from '../../shared/configuration/configuration';
import { randomBytes } from 'crypto';
import * as sanitize from 'sanitize-filename';

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, `${Configuration.basePath()}/upload/tmp`);
  },
  filename: (req, file, cb) => {
    const sanitized = sanitize(file.originalname);

    const name = sanitized.split('.')[0];
    const extension = sanitized.split('.')[1];

    const filename = randomBytes(12).toString('hex') + '_' + name.substr(0, 60 - extension.length) + '.' + extension;

    cb(null, filename);
  },
});

/**
 * Helper method for filtering files with invalid extensions via Multer.
 * @param allowedExtensions - Array of allowed extensions (e.g. ['.txt']).
 */
const checkExtension = (allowedExtensions: string[]) => {

  return (req, file, callback) => {
    const extension = path.extname(file.originalname);
    Logger.log(`Checking extension: ${extension}`);

    if (allowedExtensions.indexOf(extension) < 0) {
      Logger.log(`Checking extension: ${extension}`);
      return callback(new BadRequestException(`Invalid file type. Allowed: ${allowedExtensions.join(', ')}`), false);
    }

    return callback(null, true);
  };
};

@Controller('projects')
@ApiUseTags('Projects')
export class ProjectController {

  constructor(
    private readonly service: ProjectService,
  ) {}

  /**
   * Create new project from microscope image.
   * @param file
   * @param data
   * @param req
   */
  @Post('microscope')
  @UseGuards(AuthGuard('jwt'))
  @ApiBearerAuth()
  @ApiImplicitBody({
    name: Project.name,
    type: ProjectCreateDto,
    description: 'Data for resource creation.',
    required: true,
    isArray: false,
  })
  @ApiCreatedResponse({ type: ProjectDto })
  @ApiBadRequestResponse({ type: ApiExceptionDto })
  @ApiOperation({
    title: '[DISABLED] Create new project from microscope image.',
    operationId: 'Project_Create_Image',
    deprecated: true,
  })
  @UseInterceptors(FileInterceptor('file', {
    fileFilter: checkExtension(['.tiff']),
  }))
  async createFromImage(@UploadedFile() file, @Body() data: any, @Req() req): Promise<any> {
    return await this.service.createFromImage(data, req.user.email, file);
  }

  /**
   * Create new project from spreadsheet csv files.
   * @param file
   * @param data
   * @param req
   */
  @Post('spreadsheet')
  @UseGuards(AuthGuard('jwt'))
  @ApiBearerAuth()
  @ApiImplicitBody({
    name: Project.name,
    type: ProjectCreateDto,
    description: 'Data for resource creation.',
    required: true,
    isArray: false,
  })
  @ApiCreatedResponse({ type: ProjectDto })
  @ApiBadRequestResponse({ type: ApiExceptionDto })
  @ApiOperation({
    title: 'Create new project from data file (spreadsheet).',
    operationId: 'Project_Create_Spreadsheet',
  })
  @UseInterceptors(FileInterceptor('file', {
    storage: storage,
    fileFilter: checkExtension(['.csv']),
  }))
  async createFromSpreadsheet(@UploadedFile() file, @Body() data: any, @Req() req): Promise<any> {
    return await this.service.createFromSpreadsheet(data, req.user.email, file);
  }

  /**
   * Create new project from JSON model file.
   * @param file
   * @param data
   * @param req
   */
  @Post('model')
  @UseGuards(AuthGuard('jwt'))
  @ApiBearerAuth()
  @ApiImplicitBody({
    name: Project.name,
    type: ProjectCreateDto,
    description: 'Data for resource creation.',
    required: true,
    isArray: false,
  })
  @ApiCreatedResponse({ type: ProjectDto })
  @ApiBadRequestResponse({ type: ApiExceptionDto })
  @ApiOperation({
    title: 'Create new project from JSON model file.',
    operationId: 'Project_Create_Model',
  })
  @UseInterceptors(FileInterceptor('file', {
    fileFilter: checkExtension(['.json']),
  }))
  async createFromModel(@UploadedFile() file, @Body() data: any, @Req() req): Promise<any> {
    return await this.service.createFromModel(data, req.user.email, file);
  }

  @Get()
  @UseGuards(AuthGuard('jwt'))
  @ApiBearerAuth()
  @ApiOkResponse({ type: ProjectDto, isArray: true })
  @ApiOperation({
    title: 'Get all projects for current user',
    operationId: 'Project_GetAll',
  })
  async getAll(@Req() req): Promise<ProjectDto[]> {
    return await this.service.getAll(req.user.email);
  }

  @Get(':id')
  @UseGuards(AuthGuard('jwt'))
  @ApiBearerAuth()
  @ApiOkResponse({ type: ProjectDto })
  @ApiNotFoundResponse({ type: ApiExceptionDto })
  @ApiOperation({
    title: 'Get details of project',
    operationId: 'Project_Get',
  })
  async get(@Param('id') id: string, @Req() req): Promise<ProjectDto> {
    return this.service.get(id, req.user.email);
  }

  @Patch(':id')
  @UseGuards(AuthGuard('jwt'))
  @ApiBearerAuth()
  @ApiImplicitBody({
    name: Project.name,
    type: ProjectDto,
    description: 'Data for object update',
    required: true,
    isArray: false })
  @ApiImplicitParam({
    name: 'id',
    required: true,
    description: 'Id of the object',
    type: String })
  @ApiOkResponse({ type: ProjectDto })
  @ApiBadRequestResponse({ type: ApiExceptionDto })
  @ApiOperation({
    title: 'Update project',
    operationId: 'Project_Update' })
  async update(@Param('id') id: string, @Body() data: ProjectUpdateDto, @Req() req): Promise<any> {
    return await this.service.update(id, data, req.user.email);
  }

  @Delete(':id')
  @UseGuards(AuthGuard('jwt'))
  @ApiBearerAuth()
  @ApiImplicitParam({
    name: 'id',
    required: true,
    description: 'Id of Object',
    type: String,
  })
  @ApiOkResponse({ type: ApiSuccessDto })
  @ApiOperation({
    title: 'Remove project',
    operationId: 'Project_Delete' })
  public async delete(@Param('id') id: string, @Req() req): Promise<any> {
    try {
      return this.service.delete(id, req.user.email);
    } catch (e) {
      throw new InternalServerErrorException(e);
    }
  }

}
