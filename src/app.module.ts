import { Logger, Module } from '@nestjs/common';
import { MailerModule } from '@nest-modules/mailer';
import { SharedModule } from './shared/shared.module';
import { Configuration } from './shared/configuration/configuration';
import { TypeOrmModule } from '@nestjs/typeorm';
import { APP_FILTER, APP_INTERCEPTOR } from '@nestjs/core';
import { HttpExceptionFilter } from './shared/filters/http-exception.filter';
import { LoggingInterceptor } from './shared/interceptors/logging.interceptor';
import { AppController } from './app.controller';
import { AttachmentModule } from './api/attachment/attachment.module';
import { AuthModule } from './api/auth/auth.module';
import { UserModule } from './api/user/user.module';
import { ProjectModule } from './api/project/project.module';
import { mailerConfig } from './shared/mailer/mailer.config';
import { TicketModule } from './api/ticket/ticket.module';

@Module({
  imports: [
    TypeOrmModule.forRoot(Configuration.getDatabaseConfig()),
    MailerModule.forRoot(mailerConfig),
    SharedModule,
    /* API */
    AttachmentModule,
    AuthModule,
    UserModule,
    ProjectModule,
    TicketModule,
  ],
  controllers: [AppController],
  providers: [
    {
      provide: APP_FILTER,
      useClass: HttpExceptionFilter,
    },
    {
      provide: APP_INTERCEPTOR,
      useClass: LoggingInterceptor,
    },
  ],
})
export class AppModule {}
