import { HandlebarsAdapter } from '@nest-modules/mailer';
import { Configuration } from '../configuration/configuration';

/**
 * Configuration for mailer module
 */
export const mailerConfig = {
  transport: {
    host: process.env.MAILER_HOST,
    port: process.env.MAILER_PORT,
    secure: true,
    auth: {
      user: process.env.MAILER_USER,
      pass: process.env.MAILER_PASSWORD,
    },
  },
  defaults: {
    forceEmbeddedImages: true,
    from: `"Viewer" <${Configuration.adminEmail}>`,
  },
  template: {
    dir: `/${Configuration.basePath()}/templates`,
    adapter: new HandlebarsAdapter(),
    options: {
      doctype: 'html',
    },
  },
};
