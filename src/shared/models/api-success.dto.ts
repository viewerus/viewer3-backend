import { ApiModelPropertyOptional } from '@nestjs/swagger';

export class ApiSuccessDto {
  @ApiModelPropertyOptional() success: number;
  @ApiModelPropertyOptional() message?: string;
  @ApiModelPropertyOptional() data?: object | object[] ;
}
