import { ApiModelPropertyOptional } from '@nestjs/swagger';

export class ApiExceptionDto {
  @ApiModelPropertyOptional() code: number;
  @ApiModelPropertyOptional() message?: string;
  @ApiModelPropertyOptional() status?: string;
  @ApiModelPropertyOptional() error?: string;
  @ApiModelPropertyOptional() errors?: any;
  @ApiModelPropertyOptional() timestamp?: string;
  @ApiModelPropertyOptional() path?: string;
  @ApiModelPropertyOptional() method?: string;
}
