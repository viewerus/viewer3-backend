import { Injectable } from '@nestjs/common';
import { TypeOrmModuleOptions } from '@nestjs/typeorm';
import { DatabaseType } from 'typeorm';

/**
 * Simple class used to gain access to configuration variables.
 */
@Injectable()
export class Configuration {
  static host: string = process.env.HOST;
  static port: number = parseInt(process.env.PORT, 10);
  static env: string = process.env.NODE_ENV;
  static apiVersion: string = process.env.API_VER;

  static tokenSecret: string = process.env.tokenSecret;
  static website: string = process.env.website;
  static adminEmail: string = process.env.adminEmail;

  // Session
  static sessionSecret: string = process.env.sessionSecret;
  static sessionDuration: number = Number.parseInt(process.env.sessionDuration, 10);
  static loginAttemptsLimit: number = Number.parseInt(process.env.loginAttempts, 10);
  static lockTime: number = Number.parseInt(process.env.lockTime, 10);

  static mimeTypes = {
    html: 'text/html',
    txt: 'text/plain',
    css: 'text/css',
    gif: 'image/gif',
    jpg: 'image/jpeg',
    png: 'image/png',
    svg: 'image/svg+xml',
    js: 'application/javascript',
  };

  constructor() {}

  /**
   * Simple wrapper to get full domain with port
   */
  static getDomain() {
    return Configuration.isDevelopment() ?
      `${Configuration.host}:${Configuration.port}` :
      Configuration.host;
  }

  static basePath() {
    return process.cwd();
  }

  /**
   * Prepare config object for TypeORM based on env variables or defaults
   */
  static getDatabaseConfig(): TypeOrmModuleOptions {

    const dbConfig =  {
      name: 'default',
      type: process.env.DB_TYPE as DatabaseType,
      host: process.env.DB_HOST,
      port: process.env.DB_PORT,
      username: process.env.DB_USERNAME,
      password: process.env.DB_PASSWORD,
      database: process.env.DB_DATABASE,
      synchronize: true, // Update tables on entities change
      dropSchema: false, // Drop tables on restart
      logging: true, // Log queries to console
      // Where to look for entity files
      entities: [`${Configuration.basePath()}/{src,dist}/**/**.entity.{ts,js}`],
    };

    // Drop all tables on every initialization
    const dropTables = false;
    if (Configuration.isDevelopment() && dropTables) {
      dbConfig.dropSchema = true;
    }

    // @ts-ignore
    return dbConfig;
  }

  /**
   * Wrapper to check if we are in development mode
   */
  static isDevelopment(): boolean {
    return Configuration.env === 'development';
  }
}
