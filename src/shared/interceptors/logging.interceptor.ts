import {
  Injectable,
  NestInterceptor,
  ExecutionContext,
  Logger,
} from '@nestjs/common';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';

/**
 * Logs every incoming request
 */
@Injectable()
export class LoggingInterceptor implements NestInterceptor {

  intercept(
    context: ExecutionContext,
    call$: Observable<any>,
  ): Observable<any> {

    // Get context
    const now = Date.now();
    const req = context.switchToHttp().getRequest();
    const method = req.method;
    const url = req.url;

    // After request log details
    return call$.pipe(
      tap(() =>
        Logger.log(
          `${method} ${url} (${Date.now() - now}ms)`,
          context.getClass().name,
        ),
      ),
    );

  }
}
