import {
  ExceptionFilter,
  Catch,
  ArgumentsHost,
  Logger,
  HttpException,
  HttpStatus,
} from '@nestjs/common';
import { ApiExceptionDto } from '../models/api-exception.dto';

/**
 * Generic error filter to display, log and respond to errors
 */
@Catch()
export class HttpExceptionFilter implements ExceptionFilter {
  catch(exception: HttpException, host: ArgumentsHost) {
    Logger.error(exception);

    // Get error context
    const ctx = host.switchToHttp();
    const response = ctx.getResponse();
    const request = ctx.getRequest();
    const status = exception.getStatus
      ? exception.getStatus()
      : HttpStatus.INTERNAL_SERVER_ERROR;

    const errorMsg = this.getErrorMessages(exception);

    // Create error response
    const errorResponse: ApiExceptionDto = {
      code: status,
      timestamp: new Date().toLocaleString(),
      path: request.url,
      method: request.method,
      message: errorMsg
          || exception.message.error
          || exception.message
          || 'Internal server error',
    };

    // Show stacktrace on internal error
    if (status === HttpStatus.INTERNAL_SERVER_ERROR) {
      Logger.error(
        `${status} ${request.method} ${request.url}`,
        exception.stack,
        'ExceptionFilter',
      );
    } else {
      Logger.error(
        `${status} ${request.method} ${request.url}`,
        JSON.stringify(errorResponse),
        'ExceptionFilter',
      );
    }

    // Send error response to client
    response.status(status).json(errorResponse);
  }

  getErrorMessages(error: HttpException): string[] {
    if (!error.message.message) return;
    if (error.getStatus() !== 400) return;
    if (!error.message.message.constraints) return [error.message.message];

    const messages: string[] = [];
    for (const message of error.message.message) {
      const msg = message.constraints
                  && message.constraints[Object.keys(message.constraints)[0]];
      messages.push(msg);
    }

    return messages;
  }

}
