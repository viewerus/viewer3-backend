import {registerDecorator, ValidationOptions, ValidationArguments} from 'class-validator';

export function MixedCase(validationOptions?: ValidationOptions) {
  return (object: object, propertyName: string) => {

    registerDecorator({
      name: 'mixedCase',
      target: object.constructor,
      propertyName,
      constraints: [],
      options: validationOptions,
      validator: {
        validate(value: string, args: ValidationArguments) {
          const isLowerCase = value === value.toLowerCase();
          const isUpperCase = value === value.toUpperCase();
          return !(isLowerCase || isUpperCase);
        },
        defaultMessage(validationArguments?: ValidationArguments): string {
          return '$property must contain upper and lower case characters.';
        },
      },
    });

  };
}

export function IncludesCharacter(characters: string[], validationOptions?: ValidationOptions) {
  return (object: object, propertyName: string) => {

    registerDecorator({
      name: 'mixedCase',
      target: object.constructor,
      propertyName,
      constraints: [],
      options: validationOptions,
      validator: {
        validate(value: string, args: ValidationArguments) {

          for (const char of characters) {
            if (value.includes(char)) {
              return true;
            }
          }

          return false;
        },
        defaultMessage(validationArguments?: ValidationArguments): string {
          return '$property must contains some special characters (!@#$%^&*?_~)';
        },
      },
    });

  };
}
