/* tslint:disable:max-line-length */
import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { Configuration } from './shared/configuration/configuration';
import { Logger, ValidationPipe } from '@nestjs/common';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { getRepository } from 'typeorm';
import * as fs from 'fs';

/**
 * Function called when starting the server
 */
async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  await seedDatabase();

  // Get current host from config
  const hostDomain: string = Configuration.getDomain();

  // Prefix all routes version path (e.g. api/v1)
  const globalPrefix: string = `/api/v${Configuration.apiVersion}`;

  // Setup swagger config
  const swaggerOptions = new DocumentBuilder()
    .setTitle('Viewer')
    .setDescription('API documentation for Viewer application.')
    .setVersion('3.0.0')
    .setHost(hostDomain.split('//')[1])
    .setSchemes(Configuration.isDevelopment() ? 'http' : 'https')
    .setBasePath(globalPrefix)
    .addBearerAuth('Authorization', 'header')
    .build();

  // Create swagger docs object (json)
  const swaggerDoc = SwaggerModule.createDocument(app, swaggerOptions);

  // Display Swagger-UI for generated docs
  SwaggerModule.setup(`${globalPrefix}/docs`, app, swaggerDoc, {
    swaggerUrl: `${hostDomain}${globalPrefix}/docs-json`,
    explorer: true,
    swaggerOptions: {
      docExpansion: 'list',
      filter: true,
      showRequestDuration: true,
    },
  });

  // Global settings
  app.setGlobalPrefix(globalPrefix);
  app.useGlobalPipes(new ValidationPipe({
    whitelist: true,
    forbidUnknownValues: true,
  }));
  app.disable('x-powered-by');

  // CORS
  app.enableCors();

  // Start server
  await app.listen(Configuration.port);
  Logger.log(`Server ready: ${hostDomain}`, 'Bootstrap');
  Logger.log(`Documentation: ${hostDomain}${globalPrefix}/docs`, 'Bootstrap');
}

bootstrap().then();

async function seedDatabase() {

  // Load seed
  // TODO would be better to have separate seed/file per table and merge them here
  const path = Configuration.basePath() + '/seed/' + 'seed.json';
  const seeds = JSON.parse(fs.readFileSync(path, 'utf8'));

  for (const seed of seeds) {
    Logger.log(`${seed.table}: ${JSON.stringify(seed.rows)}`, 'SEED');
    try {
      // TODO alternatively iterate rows and check if exist first
      await getRepository(seed.table).save(seed.rows);
    } catch (e) {
      Logger.error(`Error executing seed ${e}`, 'SEED')
    }
  }

}
