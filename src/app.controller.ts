import { Get, Controller, Post } from '@nestjs/common';
import { ApiUseTags, ApiOkResponse, ApiServiceUnavailableResponse, ApiOperation, ApiModelPropertyOptional } from '@nestjs/swagger';
import { ApiExceptionDto } from './shared/models/api-exception.dto';
import { ApiSuccessDto } from './shared/models/api-success.dto';

@Controller()
@ApiUseTags('Root')
export class AppController {
  constructor() {}

  @Get()
  @ApiOperation({
    title: 'Check if server is running.',
    operationId: 'Root_Status',
  })
  @ApiOkResponse({ type: ApiSuccessDto})
  @ApiServiceUnavailableResponse({ type: ApiExceptionDto })
  serverStatus(): ApiSuccessDto {
    return {
      success: 200,
      message: 'server up',
    };
  }

}
