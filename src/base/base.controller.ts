import {
  Get,
  Post,
  Delete,
  Body,
  Param,
  Query,
  UseGuards,
  InternalServerErrorException, Patch,
} from '@nestjs/common';
import {
  ApiUseTags,
  ApiBearerAuth,
  ApiImplicitQuery,
  ApiOkResponse,
  ApiImplicitParam,
  ApiImplicitBody,
} from '@nestjs/swagger';
import { AuthGuard } from '@nestjs/passport';
import { Authenticate } from './decorators/authenticate.decorator';
import { ApiSwaggerOperation } from './decorators/swagger.decorator';
import { getAuthObj } from './utils/get-auth-obj.util';
import { AbstractEntityType, UpdateResultType, DeleteResultType } from './types/abstract.type';
import { SwaggerControllerOptions } from './types/controller-options.interface';
import { BaseService } from './base.service';
import { BaseEntity } from './base.entity';

export function BaseControllerFactory<
  T extends BaseEntity,
  VM = Partial<T>,
  C = Partial<T>
>(options: SwaggerControllerOptions<T, VM, C>): any {

  const AUTH_GUARD_TYPE = 'jwt';

  const { model, modelResponse, modelCreate } = options;
  const auth = getAuthObj(options.auth);

  @ApiUseTags(model.name)
  abstract class BaseController {
    protected readonly _service: BaseService<T>;

    protected constructor(service: BaseService<T>) {
      this._service = service;
    }

    /* --------------------------------------------------- */
    @Get()
    @Authenticate(!!auth && auth.find, UseGuards(AuthGuard(AUTH_GUARD_TYPE)))
    @Authenticate(!!auth && auth.find, ApiBearerAuth())
    @ApiImplicitQuery({
      name: 'filter',
      description: 'Find Query',
      required: false,
      isArray: false,
    })
    @ApiOkResponse({ type: modelResponse, isArray: true })
    @ApiSwaggerOperation({ title: 'FindAll' })
    public async find(@Query('filter') filter: string): Promise<T[]> {
      const findFilter = filter ? JSON.parse(filter) : {};
      try {
        return this._service.find(findFilter);
      } catch (e) {
        throw new InternalServerErrorException(e);
      }
    }

    /* --------------------------------------------------- */
    @Get(':id')
    @Authenticate(
      !!auth && auth.findById,
      UseGuards(AuthGuard(AUTH_GUARD_TYPE)),
    )
    @Authenticate(!!auth && auth.findById, ApiBearerAuth())
    @ApiImplicitParam({
      name: 'id',
      required: true,
      description: 'Id of Object',
      type: String,
    })
    @ApiOkResponse({ type: modelResponse })
    @ApiSwaggerOperation({ title: 'FindById' })
    public async findById(@Param('id') id: string): Promise<T> {
      try {
        return this._service.findById(id);
      } catch (e) {
        throw new InternalServerErrorException(e);
      }
    }

    /* --------------------------------------------------- */
    @Post()
    @Authenticate(!!auth && auth.create, UseGuards(AuthGuard(AUTH_GUARD_TYPE)))
    @Authenticate(!!auth && auth.create, ApiBearerAuth())
    @ApiImplicitBody({
      name: modelCreate.name,
      type: modelCreate,
      description: 'Data for model creation',
      required: true,
      isArray: false,
    })
    @ApiOkResponse({ type: modelResponse })
    @ApiSwaggerOperation({ title: 'Create' })
    public async create(@Body() doc: C): Promise<T> {
      try {
        const newObject = new model(doc);
        return this._service.create(newObject as AbstractEntityType<T>);
      } catch (e) {
        throw new InternalServerErrorException(e);
      }
    }

    /* --------------------------------------------------- */
    @Patch(':id')
    @Authenticate(!!auth && auth.update, UseGuards(AuthGuard(AUTH_GUARD_TYPE)))
    @Authenticate(!!auth && auth.update, ApiBearerAuth())
    @ApiImplicitBody({
      name: model.name,
      type: modelResponse,
      description: 'Data for object update',
      required: true,
      isArray: false,
    })
    @ApiImplicitParam({
      name: 'id',
      required: true,
      description: 'Id of Object',
      type: String,
    })
    @ApiOkResponse({ type: modelResponse })
    @ApiSwaggerOperation({ title: 'Update' })
    public async update(
      @Param('id') id: string,
      @Body() doc: Partial<T>,
    ): Promise<UpdateResultType<T>> {
      try {
        const existed = await this._service.findById(id);
        const updatedDoc = { ...(existed as any), ...(doc as any) } as any;
        return this._service.update(id, updatedDoc);
      } catch (e) {
        throw new InternalServerErrorException(e);
      }
    }

    /* --------------------------------------------------- */
    @Delete(':id')
    @Authenticate(!!auth && auth.delete, UseGuards(AuthGuard(AUTH_GUARD_TYPE)))
    @Authenticate(!!auth && auth.delete, ApiBearerAuth())
    @ApiImplicitParam({
      name: 'id',
      required: true,
      description: 'Id of Object',
      type: String,
    })
    @ApiOkResponse({ type: modelResponse })
    @ApiSwaggerOperation({ title: 'Delete' })
    public async delete(@Param('id') id: string): Promise<DeleteResultType<T>> {
      try {
        return this._service.delete(id);
      } catch (e) {
        throw new InternalServerErrorException(e);
      }
    }
  }

  return BaseController;
}
