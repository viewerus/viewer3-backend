export interface DefaultAuthOptions {
  find?: boolean;
  findById?: boolean;
  create?: boolean;
  update?: boolean;
  delete?: boolean;
}
