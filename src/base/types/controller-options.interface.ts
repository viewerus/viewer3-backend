import { DefaultAuthOptions } from './auth-options.interface';

export interface ControllerOptions<T> {
  model: { new (entity?: any): T };
}

export interface AuthControllerOptions<T>
  extends ControllerOptions<T> {
  auth: DefaultAuthOptions | boolean;
}

export interface SwaggerControllerOptions<T, R, C>
  extends AuthControllerOptions<T> {
  modelResponse: { new (): R };
  modelCreate: { new (): C };
}
