import { DeepPartial, UpdateResult, DeleteResult } from 'typeorm';
import { BaseEntity } from '../base.entity';
import { v4 } from 'uuid/interfaces';

export type IdType = string | number;

export type AbstractEntityType<T> = T extends BaseEntity
  ? T & DeepPartial<T>
  : any;

export type UpdateResultType<T> = T extends BaseEntity
  ? UpdateResult
  : any;

export type DeleteResultType<T> = T extends BaseEntity
  ? DeleteResult
  : any;
