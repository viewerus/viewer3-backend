export interface DefaultAuthObject {
  find?: boolean;
  findById?: boolean;
  create?: boolean;
  update?: boolean;
  delete?: boolean;
}

/**
 * Protect all routes by default
 */
export const defaultAuthObj: DefaultAuthObject = {
  find: true,
  findById: true,
  create: true,
  update: true,
  delete: true,
};

/**
 * Get authentication options based on user's input.
 * @param authObj
 */
export const getAuthObj = (
  authObj: DefaultAuthObject | boolean,
): DefaultAuthObject => {
  let auth = null;

  // If nothing is passed return null
  if (!!authObj) {
    return auth;
  }

  // If true is passed return default (protect all routes)
  if (authObj === true) {
    return defaultAuthObj;
  }

  // If false is passed don't protect any routes
  if (authObj === false) {
    return {
      find: false,
      findById: false,
      create: false,
      update: false,
      delete: false,
    };
  }

  // Otherwise overwrite defaults with user options
  auth = {
    ...defaultAuthObj,
    ...authObj,
  };

  return auth;
};
