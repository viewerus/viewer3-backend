import { AbstractEntityType, DeleteResultType, IdType, UpdateResultType } from './types/abstract.type';
import { Repository, FindManyOptions, FindConditions } from 'typeorm';
import { BaseEntity } from './base.entity';

export class BaseService<T extends BaseEntity> {
    protected _repository: Repository<T>;

    constructor(model: Repository<T>) {
        this._repository = model;
    }

    public async find(
        filter: FindManyOptions<T> & FindConditions<T> = {},
    ): Promise<T[]> {
        return this._repository.find(filter);
    }

    public async findById(id: IdType): Promise<T> {
        return this._repository.findOne(id);
    }

    public async findOne(filter: FindConditions<T> = {}): Promise<T> {
        return this._repository.findOne(filter);
    }

    public async create(doc: AbstractEntityType<T>): Promise<T> {
        return this._repository.save(doc as any);
    }

    public async update(
        id: IdType,
        updatedDoc: AbstractEntityType<T>,
    ): Promise<UpdateResultType<T>> {
        return this._repository.update(
            id,
            updatedDoc as any,
        ) as UpdateResultType<T>;
    }

    public async delete(id: IdType): Promise<DeleteResultType<T>> {
        return this._repository.delete(id) as DeleteResultType<T>;
    }

}
