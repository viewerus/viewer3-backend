FROM node:dubnium-alpine as dist
WORKDIR /tmp/
COPY package.json package-lock.json tsconfig.json ./
COPY src src/
RUN npm install
RUN npm run build

FROM node:dubnium-alpine as node_modules
WORKDIR /tmp/
COPY package.json package-lock.json ./
RUN npm install --production

FROM node:dubnium-alpine
WORKDIR /usr/local/api
COPY --from=node_modules /tmp/node_modules ./node_modules
COPY --from=dist /tmp/dist ./dist

RUN mkdir -p -m 664  upload
RUN mkdir -p -m 664  upload/tmp
COPY upload/human.json upload/miskant.json ./upload/
COPY seed ./seed
COPY templates ./templates
CMD ["node", "dist/main.js"]
