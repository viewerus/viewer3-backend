#!/bin/sh

fake_certs=0

main() {
    prepareConfig
    waitForCertificates
    startServer
    scheduleReload
}

# Replace env variables in config files and save results
prepareConfig() {
    printf "\n\n Replacing env variables in templates..."
    # TODO make it more generic, iterate through all *.template files (param needed: which env vars to replace)
    envsubst '${NGINX_HOST}' < /etc/nginx/templates/default.template > /etc/nginx/sites/domain.conf
}

waitForCertificates() {
    printf "\n\n Checking certificates... \n"
    ls -la /etc/letsencrypt

    requiredFiles=(/etc/letsencrypt/privkey.pem /etc/letsencrypt/fullchain.pem /etc/letsencrypt/dhparam.pem)

    if [[ ! -f ${requiredFiles[0]} && ! -f ${requiredFiles[1]} && ! -f ${requiredFiles[2]} ]]; then
            printf "\t No certificates present. \n"
    fi

    while : ; do
        if [[ -f ${requiredFiles[0]} && -f ${requiredFiles[1]} && -f ${requiredFiles[2]} ]]; then
            printf "\t Certificates found. \n"

            certbotFlag=/etc/letsencrypt/certs-ready.txt
            if [[ ! -f ${certbotFlag} ]]; then
                printf "\t Certificates are self-signed! \n"
                fake_certs=1
            fi

            break
        fi
        sleep 1
    done
}

# Wait for certbot to generate real certs and reload the server
reloadAfterCertbot() {
    printf "\n\n Waiting for real certificates... \n"
    certbotFlag=/etc/letsencrypt/certs-ready.txt

    while : ; do

        if [[ -f ${certbotFlag} ]]; then
            printf "\t Certbot's certificates ready. \n"
            nginx -s reload;
            return;
        fi

        printf "\t Certbot's certificates not found... \n"
        sleep 5

    done
}

# Every day at 4AM reload config and certs
scheduleReload() {
    # Every day at 2AM -> renew certificates
    printf "\n Setting schedule for server reload... \n\n"

    touch -a /etc/certbot.log
    (crontab -l && echo "0 4 * * *  nginx -s reload >> /etc/nginx.log") | crontab -
    crond

    tail -f /etc/certbot.log

}

startServer() {
    printf "\n Starting the server... \n"
    nginx

    if [[ fake_certs -eq 1 ]]; then
        reloadAfterCertbot
    fi
}

main
