#!/bin/bash

# Global variables
email=""
domains=()
testing=0
webRoot="/var/www/certbot"
outDir="/var/certs"

main() {
  getArguments "$@"
  generateDummyCerts
  generateDH
  waitForServer
  generateCerts
  scheduleRenewal
  printf "\n\n Finished! \n\n"
}

usage() {
    printf "\n Usage: \`$0 -e 'your@email.com' -d 'domain.com, sub.domain.com' -t\`";
    printf "\n -e | --email   \t Your email. Used to notify you about expired certificates.";
    printf "\n -d | --domains \t Comma separated list of domain (exclude www) for ssl certificate. The 'www.*' domains are added.";
    printf "\n -t | --testing \t Testing flag. If set staging server will be used (use for testing!)."
    printf "\n -h | --help    \t Shows this usage message. \n\n"
    exit 1;
}

getArguments() {
    printf "\n Loading arguments... \n"

    while [[ "$1" =~ ^- && ! "$1" == "--" ]]; do
        case $1 in
            -e | --email )
                shift;
                email=$1
                ;;
            -d | --domains )
                shift;
                domainsString=$1
                ;;
            -t | --testing )
                testing=1
                ;;
            -h | --help )
                usage
                ;;
        esac;
        shift;
    done
    if [[ "$1" == '--' ]]; then shift; fi

    if [[ -z "${email}" ]] || [[ -z "${domainsString}" ]]; then
        usage
    fi

    # Parse domains
    IFS=', ' read -r -a domains <<< "${domainsString}"
    for domain in "${domains[@]}"; do
        domains+=("www.${domain}") # Prepend www.*
    done

    printf "\t Email: ${email} \n"
    printf "\t Domains: ${domains} \n"
    printf "\t Testing: ${testing} \n"
}

generateDummyCerts() {
    printf "\n Checking certificates... "

    path="/etc/letsencrypt/${domains[0]}/live"
    mkdir -p ${outDir}

    if [[ -f "${outDir}/privkey.pem" && -f "${outDir}/fullchain.pem" ]]; then
        printf "\n\t Certificates already present. Do nothing. \n"
        return;
    fi

    printf "\n\t No certificates detected. Generating self-signed certs with openssl. \n"
    openssl req -x509 -nodes -days 1 -newkey rsa:1024 \
        -keyout "${outDir}/privkey.pem" \
        -out "${outDir}/fullchain.pem" \
        -subj "/CN=localhost"
}

generateDH() {
    printf "\n Checking Diffie–Hellman file..."
    dhparam_file="${outDir}/dhparam.pem"
    if [[ ! -f "$dhparam_file" ]]
    then
        mkdir -p /etc/letsencrypt/
        printf "\n\t Not found. Generating Diffie–Hellman key exchange file. %s \n" $(date +%T)

        openssl dhparam -out /etc/letsencrypt/dhparam.pem 2048 2> /dev/null
        cp -v /etc/letsencrypt/dhparam.pem ${outDir}/dhparam.pem

        printf "\n\t Diffie–Hellman file generated. %s \n" $(date +%T)
    else
        printf "\n\t File exists, generation skipped. \n"
    fi
}

waitForServer() {
    printf "\n Waiting for NGINX server..."

    server=1
    wget --tries=1 --quiet --spider --no-cache --no-check-certificate "http://${domains[0]}/.well-known/healthcheck" || server=0

    if [[ ${server} -eq 1 ]]; then printf "\n\t Server ready. \n"; return; fi

    while [[ "$server" -eq 0 ]]; do
        printf "\n\t No response. \n"
        server=1
        wget --tries=1 --quiet --spider --no-cache --no-check-certificate "http://${domains[0]}/.well-known/healthcheck" || server=0
        sleep 5
    done

    printf "\n\t Server ready. \n";
}

generateCerts() {

    # Check if there is need...
    requiredFiles=(${outDir}/certs-ready.txt ${outDir}/fullchain.pem ${outDir}/privkey.pem)
    if [[ -f ${requiredFiles[0]} && -f ${requiredFiles[1]} && -f ${requiredFiles[2]} ]]; then
        printf "\n Certs already present, skipping generation. \n";
        return;
    fi

    printf "\n Generating new certificates..."

    # prepare arguments
    if [[ ${testing} -eq 1 ]]; then useStaging="--staging"; fi

    domainArgs=""
    for domain in "${domains[@]}"; do
        domainArgs="$domainArgs -d $domain"
    done

    mkdir -p ${webRoot}
    certbot certonly --webroot --agree-tos --verbose --noninteractive --quiet \
            ${useStaging} \
            ${domainArgs} \
            -w ${webRoot} \
            -m ${email};

    cp -v /etc/letsencrypt/live/${domains[0]}/privkey.pem ${outDir}
    cp -v /etc/letsencrypt/live/${domains[0]}/fullchain.pem ${outDir}

    # Add flag to let server not certs are ready
    touch ${outDir}/certs-ready.txt
}

scheduleRenewal() {
    # Every day at 2AM -> renew certificates
    printf "\n Setting schedule for certificate renewal... \n\n"

    cronCommand="certbot renew && "
    cronCommand+="cp -v /etc/letsencrypt/live/${domains[0]}/privkey.pem ${outDir} && "
    cronCommand+="cp -v /etc/letsencrypt/live/${domains[0]}/fullchain.pem ${outDir};"

    touch -a /etc/certbot.log
    (crontab -l && echo "0 4 * * *  { ${cronCommand} } >> /etc/certbot.log") | crontab -
    crond

    tail -f /etc/certbot.log
}

main "$@"; exit
